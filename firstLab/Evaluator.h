//
//  Evaluator.h
//  firstLab
//
//  Created by Danila Kolesnikov on 1/3/13.
//  Copyright (c) 2013 dandandan. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PlotData;

@interface Evaluator : NSObject

@property(retain) PlotData *x1data;
@property(retain) PlotData *x2data;
@property(retain) PlotData *x3data;
@property(retain) PlotData *precision;
@property(retain) PlotData *labority;

@end