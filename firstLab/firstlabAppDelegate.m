//
//  firstlabAppDelegate.m
//  firstLab
//
//  Created by Danila Kolesnikov on 12/31/12.
//  Copyright (c) 2012 dandandan. All rights reserved.
//

#import "firstlabAppDelegate.h"
#import "Evaluator.h"
#import "PlotData.h"

#define RED    [CPTColor colorWithComponentRed:1.0 green:0.0 blue:0.0 alpha:1.0]
#define GREEN  [CPTColor colorWithComponentRed:0.0 green:1.0 blue:0.0 alpha:1.0]
#define BLUE   [CPTColor colorWithComponentRed:0.0 green:0.0 blue:1.0 alpha:1.0]

#define HELP        @"∂x1/∂t = x2\n∂x2/∂t = cu/x3 - rx2ˆ2/x3\n∂x3/∂t = -u\nc = 4000, u = 40, g = 9.81\nНУ: x1 = x2 = 0, x3 = 1200"
#define HELP_HEADER @"Вариант 3:"

#define X1        1
#define X2        2
#define X3        3
#define PRECISION 4
#define LABORITY  5

@interface firstlabAppDelegate()
-(CPTGraph *)createGraphMaxX:(double)maxX andMaxY:(double)maxY;
-(PlotData *)getDataForPlot:(CPTPlot *)plot;
@end


@implementation firstlabAppDelegate

- (void)dealloc {
    [super dealloc];
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {

    NSColor *semiTransparentBlue =
    [NSColor colorWithDeviceRed:0.5 green:0.5 blue:0.5 alpha:1.0];
    [self.window setBackgroundColor:semiTransparentBlue];
    
    graphView.hostedGraph          = [self createGraphStartX:-1.0
                                                   andStartY:-1000.0
                                                     andMaxX:12
                                                     andMaxY:10000];
    
    precisionGraphView.hostedGraph = [self createGraphStartX:-0.0014
                                                   andStartY:-0.00005
                                                     andMaxX:0.014
                                                     andMaxY:0.0005];
    
    laborityGraphView.hostedGraph  = [self createGraphStartX:-0.009
                                                   andStartY:-500
                                                     andMaxX:0.1
                                                     andMaxY:8000];
    

    
    [graphView.hostedGraph addPlot:[self createPlotWithIdentifier:[NSNumber numberWithInt:X1] andColor:RED]];
    [graphView.hostedGraph addPlot:[self createPlotWithIdentifier:[NSNumber numberWithInt:X2] andColor:GREEN]];
    [graphView.hostedGraph addPlot:[self createPlotWithIdentifier:[NSNumber numberWithInt:X3] andColor:BLUE]];
    [precisionGraphView.hostedGraph addPlot:[self createPlotWithIdentifier:[NSNumber numberWithInt:PRECISION] andColor:GREEN]];
    [laborityGraphView.hostedGraph addPlot:[self createPlotWithIdentifier:[NSNumber numberWithInt:LABORITY] andColor:BLUE]];
    
    
    evaluator = [[Evaluator alloc] init];
}

-(CPTScatterPlot *) createPlotWithIdentifier:(id)ident
                                    andColor:(CPTColor *)color {
    CPTScatterPlot *x1Plot = [[[CPTScatterPlot alloc] init ] autorelease];
    
    x1Plot.dataSource = self;
    x1Plot.identifier = ident;
    x1Plot.interpolation = CPTScatterPlotInterpolationCurved;

    CPTMutableLineStyle *lineStyle = [[x1Plot.dataLineStyle mutableCopy] autorelease];
    lineStyle.lineColor = color;
    lineStyle.lineWidth = 2.0;
    x1Plot.dataLineStyle = lineStyle;
    return x1Plot;
}


-(CPTGraph *)createGraphStartX:(double)startX
                     andStartY:(double)startY
                       andMaxX:(double)maxX
                       andMaxY:(double)maxY {
    CPTXYGraph *graph = [(CPTXYGraph *)[CPTXYGraph alloc] initWithFrame:CGRectZero];
    CPTTheme *theme = [CPTTheme themeNamed:kCPTDarkGradientTheme];
    [graph applyTheme:theme];
    CPTXYPlotSpace *space = (CPTXYPlotSpace *) graph.defaultPlotSpace;
    space.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(startX)
                                                length:CPTDecimalFromFloat(maxX)];
    
    space.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(startY)
                                                length: CPTDecimalFromFloat(maxY)];
    CPTXYAxisSet *axis = (CPTXYAxisSet *)graph.axisSet;

    axis.yAxis.minorTicksPerInterval       = 5;
    axis.yAxis.majorIntervalLength         = CPTDecimalFromString([NSString stringWithFormat:@"%f", maxY / 10]);
    
    axis.xAxis.minorTicksPerInterval       = 5;
    axis.xAxis.majorIntervalLength         = CPTDecimalFromString([NSString stringWithFormat:@"%f", maxX / 10]);
    
    [axis.yAxis.labelFormatter setMaximumFractionDigits:5];
    [axis.xAxis.labelFormatter setMaximumFractionDigits:5];
    
    return [graph autorelease];
}

- (IBAction)showHelp:(id)sender {
    NSAlert *alert = [[NSAlert alloc] init];
    [alert addButtonWithTitle:@"OK"];
    [alert setMessageText:HELP_HEADER];
    [alert setInformativeText:HELP];
    [alert setAlertStyle:NSWarningAlertStyle];
    [alert runModal];
}

-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot {
    PlotData *plotData = [self getDataForPlot:plot];
    return plotData.data.count;
}

-(NSNumber *)numberForPlot:(CPTPlot *)plot
                     field:(NSUInteger)fieldEnum
               recordIndex:(NSUInteger)index {
    
    PlotData *plotData = [self getDataForPlot:plot];
    
    NSNumber *num = [[plotData.data objectAtIndex:index]
                                       objectForKey:[NSNumber numberWithInt:fieldEnum]];
    
    return num;

}

-(PlotData *)getDataForPlot:(CPTPlot *)plot {
    PlotData *plotData;
    NSNumber *number = (NSNumber *)plot.identifier;
    switch ([number integerValue]) {
        case X1:
            plotData = evaluator.x1data;
            break;
        case X2:
            plotData = evaluator.x2data;
            break;
        case X3:
            plotData = evaluator.x3data;
            break;
        case LABORITY:
            plotData = evaluator.labority;
            break;
        case PRECISION:
            plotData = evaluator.precision;
            break;
        default:
            break;
    }
    return plotData;
}



@end
