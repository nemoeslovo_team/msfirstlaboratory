//
//  PlotData.m
//  firstLab
//
//  Created by Danila Kolesnikov on 1/3/13.
//  Copyright (c) 2013 dandandan. All rights reserved.
//

#import "PlotData.h"
#import <CorePlot/CorePlot.h>

@implementation PlotData

@synthesize data = _data;
@synthesize step = _step;

- (void) dealloc {
    [_data release];
    [super dealloc];
}

- (id) init {
    self = [super init];
    if (self != nil) {
        _data = [[NSMutableArray alloc] init];
        _step = 0;
    }
    return self;
}

- (void)addX:(double)x
        andY:(double)y {
    
    
    NSNumber *firstObject  = [NSNumber numberWithDouble:x];
    NSNumber *firstKey     = [NSNumber numberWithInt:CPTScatterPlotFieldX];
    NSNumber *secondObject = [NSNumber numberWithDouble:y];
    NSNumber *secondKey    = [NSNumber numberWithInt:CPTScatterPlotFieldY];
    
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys: firstObject
                                                , firstKey
                                                , secondObject
                                                , secondKey
                                                , nil];
    
    [self.data addObject:dictionary];
}

- (void)clear {
    [self.data removeAllObjects];
}

@end