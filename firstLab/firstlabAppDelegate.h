//
//  firstlabAppDelegate.h
//  firstLab
//
//  Created by Danila Kolesnikov on 12/31/12.
//  Copyright (c) 2012 dandandan. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <CorePlot/CorePlot.h>

@class Evaluator;

@interface firstlabAppDelegate : NSObject <NSApplicationDelegate, CPTPlotDataSource> {
    Evaluator *evaluator;

    IBOutlet CPTGraphHostingView *precisionGraphView;
    IBOutlet CPTGraphHostingView *graphView;
    IBOutlet CPTGraphHostingView *laborityGraphView;

}

- (IBAction)showHelp:(id)sender;

@property (assign) IBOutlet NSWindow *window;

@end
