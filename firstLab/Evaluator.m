//
//  Evaluator.m
//  firstLab
//
//  Created by Danila Kolesnikov on 1/3/13.
//  Copyright (c) 2013 dandandan. All rights reserved.
//

#import "Evaluator.h"
#import "PlotData.h"

@implementation Evaluator

@synthesize x1data = _x1data;
@synthesize x2data = _x2data;
@synthesize x3data = _x3data;
@synthesize precision = _precision;
@synthesize labority  = _labority;

- (void)dealloc {
    [_x1data release];
    [_x2data release];
    [_x3data release];
    [_precision release];
    [_labority  release];
    
    [super dealloc];
}

- (id)init {
    self = [super init];
    if (self != nil) {
        _x1data = [[PlotData alloc] init];
        _x2data = [[PlotData alloc] init];
        _x3data = [[PlotData alloc] init];
        _precision = [[PlotData alloc] init];
        _labority  = [[PlotData alloc] init];
        
        [self evalWithStep:0.01
            changePlotData:YES];
        [self evalPrecisionAndLabority];
    }
    return self;
}


- (double) evalWithStep:(double)step
       changePlotData:(BOOL)graf {
    int    t0    =    0;
    int    t1    =   10;
    double x1    =    0;
    double x2    =    0;
    double x3    = 1000;
    double c     = 4000;
    double g     =    9.81;
    double u     =   40;
    double t     =   t0;
    double r     =    0;
    int    index =    0;
    int    b     =  500;
    
    double rh[] = {0.015, 0.0135, 0.012, 0.0107, 0.0096, 0.0088, 0.0077, 0.0069, 0.0061, 0.0055, 0.0049, 0.0044, 0.0039, 0.0035, 0.0031, 0.0028, 0025, 0.0022, 0.002, 0.0018, 0.0016};
    
    int k = (t1-t0) / step;
    
    if (graf) {
        [self.x1data clear];
        [self.x1data clear];
        [self.x1data clear];
    }
    
    for(int i=0; i<k; ++i, t+=step) {
        if (graf) {
            [self.x1data addX:t andY:x1];
            [self.x2data addX:t andY:x2];
            [self.x3data addX:t andY:x3];
        }
        
        x1 = x1 + x2*step;
        r  = ( (b-x1) * (rh[index] - rh[index+1]) / 500) + rh[index+1];
        x2 = x2 + (c*u/x3 - g - r*x2*x2/x3) * step;
        x3 = x3 - u*step;
        if (x1>b) {
            b += 500;
            index++;
        }
    }
    return x1; 
}

-(void)evalPrecisionAndLabority {
    double h   = 0.1;
    double x1  = 0;
    double x10 = [self evalWithStep:h changePlotData:NO];
    double E   = 0;
    bool   f   = 1;
    double nujnH=0;

    [self.precision clear];
    [self.labority clear];
    
    for(int i=0; i<15; i++) {
        h  /= 2;
        x1  = [self evalWithStep:h changePlotData:NO];
        E   = (x1-x10)/x10;
        if (f&&E <= 0.01) {
            f     =   0;
            nujnH = h*2;
        }
        x10 = x1;
        [self.precision addX:h*2
                        andY:E];
        [self.labority addX:h*2
                        andY:10/h];
    }
//    Label3->Caption=FloatToStr(nujnH);
//    Label4->Visible=true;
}


@end
