//
//  main.m
//  firstLab
//
//  Created by Danila Kolesnikov on 12/31/12.
//  Copyright (c) 2012 dandandan. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
