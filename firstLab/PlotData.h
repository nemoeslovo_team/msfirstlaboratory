//
//  PlotData.h
//  firstLab
//
//  Created by Danila Kolesnikov on 1/3/13.
//  Copyright (c) 2013 dandandan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PlotData : NSObject

@property                   int             step;
@property(readonly, retain) NSMutableArray *data;

- (void)addX:(double)x
       andY:(double)y;

- (void)clear;

@end
